package com.example.popko.activites

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.popko.R
import com.example.popko.profile.ProfileItem
import com.example.popko.profile.ProfileItemAdapter
import kotlinx.android.synthetic.main.activity_view_profile.*
import kotlinx.android.synthetic.main.activity_view_profile.toolbar
import kotlinx.android.synthetic.main.app_bar_nav_drawer.*
import kotlinx.android.synthetic.main.content_profile.*
import kotlinx.android.synthetic.main.fragment_linear_layout.*

class ViewProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.picAvatar)
        initProfile()
    }



    private fun onMapClick() {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse("geo:0,0?q=Витебск+Покровская+улица+11А")
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }

    }

    private fun onGitClick() {
        val webpage: Uri = Uri.parse(resources.getString(R.string.git_lab_profile))
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }

    }

    private fun onPhoneClick() {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:"+resources.getString(R.string.phone_profile))
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }

    }

    private fun onEmailClick() {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:") // only email apps should handle this
            putExtra(Intent.EXTRA_EMAIL, resources.getString(R.string.email_profile))
            putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.subject_email_profile))
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }

    }

    private fun initProfile(){
        val profileAdapter = ProfileItemAdapter()
        contacts_view.layoutManager = LinearLayoutManager(this)
        contacts_view.adapter = profileAdapter
        profileAdapter.profileItems = getProfileList()
    }

    private fun getProfileList(): List<ProfileItem>{


        val profileHeaders = listOf<String>(
            resources.getString(R.string.email_profile),
            resources.getString(R.string.phone_profile),
            resources.getString(R.string.git_lab_profile),
            resources.getString(R.string.map_profile)
        )
        val profileDescriptions = listOf<String>(
            resources.getString(R.string.email_profile_description),
            resources.getString(R.string.phone_profile_description),
            resources.getString(R.string.git_lab_profile_description),
            resources.getString(R.string.map_profile_description)
        )

        val profileIcons = listOf<Drawable>(
            resources.getDrawable(R.drawable.ic_mail),
            resources.getDrawable(R.drawable.ic_phone),
            resources.getDrawable(R.drawable.ic_git),
            resources.getDrawable(R.drawable.ic_map)
        )

        val profileItemClickListeners = listOf(
            View.OnClickListener { onEmailClick() },
            View.OnClickListener { onPhoneClick() },
            View.OnClickListener { onGitClick() },
            View.OnClickListener { onMapClick() }
        )


        val profileArray = arrayListOf<ProfileItem>()
        for (i in profileHeaders.indices)
            profileArray.add(ProfileItem(
                    profileHeaders[i],
                    profileDescriptions[i],
                    profileIcons[i],
                    profileItemClickListeners[i]
            ))
        return profileArray.toList()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
