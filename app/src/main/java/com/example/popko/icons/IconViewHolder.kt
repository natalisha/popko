package com.example.popko.icons

import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.linear_item_view.view.*

enum class LayoutModes() {
    TIGHT_LAYOUT,
    STANDARD_LAYOUT,
    LINEAR_LAYOUT
}

class IconViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bind(icon: ResolveInfo, packageManager:PackageManager, layoutMode: LayoutModes){
        when (layoutMode){
        LayoutModes.LINEAR_LAYOUT -> {
            itemView.tvName.text = icon.loadLabel(packageManager)
            itemView.tvSecondaryText.text = icon.activityInfo.packageName
        }
        LayoutModes.STANDARD_LAYOUT -> {
            itemView.tvName.text = icon.loadLabel(packageManager)
        }
        }
        itemView.ivAppIcon.setImageDrawable(icon.loadIcon(packageManager))
    }


}