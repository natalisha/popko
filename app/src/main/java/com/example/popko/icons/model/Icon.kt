package com.example.popko.icons.model

import android.widget.ImageView

sealed class Icon {
}

data class SimpleIcon(
        val imageView: ImageView,
        val name: String,
        val secondaryText: String
): Icon()