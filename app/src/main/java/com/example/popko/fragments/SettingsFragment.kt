package com.example.popko.fragments

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager

import com.example.popko.R
import com.example.popko.SharedPreferencesListener

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : PreferenceFragmentCompat() {
    private val preferencesListener = SharedPreferencesListener()
    private lateinit var mSettings: SharedPreferences


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.layncher_settings, rootKey)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSettings = PreferenceManager.getDefaultSharedPreferences(activity?.applicationContext)
        mSettings.registerOnSharedPreferenceChangeListener(preferencesListener)
        (activity as? AppCompatActivity)?.supportActionBar?.title = resources.getString(R.string.nav_settings)
    }

    override fun onDestroy() {
        super.onDestroy()
        mSettings.unregisterOnSharedPreferenceChangeListener(preferencesListener)
    }

}
