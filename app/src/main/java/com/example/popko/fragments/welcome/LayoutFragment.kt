package com.example.popko.fragments.welcome

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.popko.R
import com.example.popko.data.SETTINGS_LAYOUT
import kotlinx.android.synthetic.main.fragment_maket.*

class LayoutFragment : Fragment() {

    private lateinit var mSettings: SharedPreferences


    private fun highlightStandard(){
        clStandard.background = resources.getDrawable(R.drawable.choosen_rectangle, null)
        clTight.background = resources.getDrawable(R.drawable.rectangle)
    }

    private fun highlightTight(){
        clTight.background = resources.getDrawable(R.drawable.choosen_rectangle, null)
        clStandard.background = resources.getDrawable(R.drawable.rectangle)
    }
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mSettings = PreferenceManager.getDefaultSharedPreferences(context)
        return inflater.inflate(R.layout.fragment_maket, container, false)
    }

    private fun onLayoutClick(isTight: Boolean){
        btnTightLayout.isChecked = isTight
        btnStandardLayout.isChecked = !isTight
        mSettings.edit().putBoolean(SETTINGS_LAYOUT, isTight).apply()
        if (isTight){
            highlightTight()
        }else
            highlightStandard();
    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onLayoutClick(mSettings.getBoolean(SETTINGS_LAYOUT, true))

        clStandard.setOnClickListener{
            onLayoutClick(false)
        }

        clTight.setOnClickListener {
            onLayoutClick(true)
        }
        btnStandardLayout.setOnClickListener{
            onLayoutClick(false)
        }

        btnTightLayout.setOnClickListener{
            onLayoutClick(true)
        }


    }

}

