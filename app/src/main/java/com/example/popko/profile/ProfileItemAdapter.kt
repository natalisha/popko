package com.example.popko.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.popko.R

class ProfileItemAdapter: RecyclerView.Adapter<ProfileHolder>() {

    var profileItems: List<ProfileItem> = emptyList()
        set(value){
            field = value
            notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileHolder{
        return ProfileHolder(LayoutInflater.from(parent.context).inflate(R.layout.profile_item, parent, false))
    }


    override fun getItemCount(): Int = profileItems.size

    override fun onBindViewHolder(holder: ProfileHolder, position: Int) {
        holder.bind(profileItems[position])
    }
}