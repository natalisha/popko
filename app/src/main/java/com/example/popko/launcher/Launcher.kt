package com.example.popko.launcher

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ResolveInfo
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.popko.R
import com.example.popko.activites.WelcomeActivity
import com.example.popko.data.SETTINGS_START_PAGE
import com.example.popko.icons.model.ActivityStarter

class Launcher : AppCompatActivity() {

    lateinit var mSettings: SharedPreferences
    private lateinit var appBarConfiguration: AppBarConfiguration

    val activityStarter = MyActivityStarter(this)

    class MyActivityStarter(val context: Context): ActivityStarter {
        override fun startActivity(name: String) {
            context.startActivity(context.packageManager.getLaunchIntentForPackage(name)   )   }

    }

    override fun onCreate(savedInstanceState: Bundle?) {

//        generateColorList()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mSettings = PreferenceManager.getDefaultSharedPreferences(this)
        mSettings.edit().putBoolean(SETTINGS_START_PAGE, false).apply()

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                        R.id.tableLayoutFragment, R.id.linearFragment
                ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.getHeaderView(0).setOnClickListener{
            navController.navigate(R.id.viewProfileActivity)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.nav_drawer, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)

        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun getAppsList(): List<ResolveInfo> {

        return packageManager.queryIntentActivities(Intent(Intent.ACTION_MAIN, null).apply {
            addCategory(Intent.CATEGORY_LAUNCHER);
        }, 0)
    }

}
