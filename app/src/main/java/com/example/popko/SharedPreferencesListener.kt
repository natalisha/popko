package com.example.popko

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import com.example.popko.data.SETTINGS_MODE

class SharedPreferencesListener : SharedPreferences.OnSharedPreferenceChangeListener {
    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        when(p1){
            SETTINGS_MODE->AppCompatDelegate.setDefaultNightMode(
                if (p0!!.getBoolean(SETTINGS_MODE, false)) {
                    AppCompatDelegate.MODE_NIGHT_YES
                } else {
                    AppCompatDelegate.MODE_NIGHT_NO
                }
            )
        }

    }
}