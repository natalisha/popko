package com.example.popko.icons

import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.popko.icons.model.ActivityStarter
import com.example.popko.icons.model.IconModeler

class IconsClassAdapter(private val iconType: LayoutModes, private val packageManager: PackageManager,
                        private val activityStarter: ActivityStarter): RecyclerView.Adapter<IconViewHolder>() {


    var icons: List<ResolveInfo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IconViewHolder {
        val typeIcon = IconModeler.byTypeInt(iconType)
        val view = LayoutInflater.from(parent.context).inflate(typeIcon.layoutId, parent, false)
        return IconViewHolder(view)
    }

    override fun getItemCount(): Int = icons.size

    override fun onBindViewHolder(holder: IconViewHolder, position: Int) {
        holder.bind(icons[position], packageManager, iconType)
        holder.itemView.setOnClickListener{
            activityStarter.startActivity(icons[position].activityInfo.packageName)
        }
    }
}