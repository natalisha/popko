package com.example.popko.launcher

import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager

import com.example.popko.R
import com.example.popko.data.*
import com.example.popko.icons.IconsClassAdapter
import com.example.popko.icons.LayoutModes
import kotlinx.android.synthetic.main.fragment_table_layout.*



class TableLayoutFragment : Fragment() {

    private lateinit var mSettings: SharedPreferences

    private val itemType = 2
    private lateinit var iconListAdapter: IconsClassAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = resources.getString(R.string.nav_table)
        mSettings = PreferenceManager.getDefaultSharedPreferences(context)
        initList()
    }

    private fun initList() {
        val isTight: Boolean = mSettings.getBoolean(SETTINGS_LAYOUT, false)
        val spanCount = if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            if (!isTight){
                STANDARD_PORTR_DENSITY
            }else{
                TIGHT_PORTR_DENSITY
            }
        }else{
            if (!isTight){
                STANDARD_LAND_DENSITY
            }else{
                TIGHT_LAND_DENSITY
            }
        }
        var layoutMode: LayoutModes = LayoutModes.STANDARD_LAYOUT
        if (isTight){
            layoutMode = LayoutModes.TIGHT_LAYOUT
        }
        iconListAdapter = IconsClassAdapter(layoutMode,
                (activity as Launcher).packageManager,(activity as Launcher).activityStarter)
        tableRecycleView.layoutManager = GridLayoutManager(activity?.applicationContext, spanCount)
        tableRecycleView.adapter = iconListAdapter
        iconListAdapter.icons = (activity as Launcher).getAppsList()

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_table_layout, container, false)
    }


}
