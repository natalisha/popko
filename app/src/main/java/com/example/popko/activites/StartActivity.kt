package com.example.popko.activites

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.example.popko.R
import com.example.popko.data.SETTINGS_START_PAGE
import com.example.popko.launcher.Launcher

class StartActivity : AppCompatActivity() {

    lateinit var mSettings: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        setContentView(R.layout.activity_view_profile)
        mSettings = PreferenceManager.getDefaultSharedPreferences(this)
        if (mSettings.getBoolean(SETTINGS_START_PAGE, true)){
            val intent = Intent(this, WelcomeActivity::class.java)
            startActivity(intent)
        }else{
            val intent = Intent(this, Launcher::class.java)
            startActivity(intent)
        }
    }
}
