package com.example.popko.profile

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import java.io.FileDescriptor

data class ProfileItem(
        val content: String,
        val descriptor: String,
        val icon: Drawable,
        val onClickListener: View.OnClickListener
) {

}