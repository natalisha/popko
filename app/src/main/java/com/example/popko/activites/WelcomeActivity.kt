package com.example.popko.activites

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.popko.R
import com.example.popko.fragments.welcome.*
import com.example.popko.launcher.Launcher
import com.example.popko.viewPagerAdapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_welcome.*


class WelcomeActivity : AppCompatActivity(){

   //var navController: NavController? = null
    lateinit var mSettings: SharedPreferences
    private lateinit var pagerAdapter: PagerAdapter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        mSettings = PreferenceManager.getDefaultSharedPreferences(this)
//        mEditor = mSettings.edit()

//        if (mSettings.contains(SETTINGS_MODE)) {
//            if (mSettings.getBoolean(SETTINGS_MODE, false)) {
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//            } else {
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//            }
//        }
        prepareDots()

        // Create fragments on welcome page
        val fragments:List<Fragment> = listOf(
                WelcomeFragment(),
                DescriptionFragment(),
                ThemeFragment(),
                LayoutFragment()
        )

        pagerAdapter = ScrollPagesWelcomeAdapter(supportFragmentManager, fragments)
        view_pager.adapter = pagerAdapter

        // Add listener no nextButton
        navigation_button.setOnClickListener {
            with(view_pager) {
                if (currentItem < fragments.size - 1)
                    view_pager.arrowScroll(View.FOCUS_RIGHT)
                else {
                    val intent = Intent();
                    intent.setClass(applicationContext, Launcher::class.java)
                    startActivity(intent)
                }
            }
        }

        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                prepareDots()
            }

        })

    }

    override fun onBackPressed() {

        if (view_pager.currentItem != 0) {
            view_pager.arrowScroll(View.FOCUS_LEFT)
        } else
            super.onBackPressed()
    }

    private fun prepareDots() {
        if (pageIndicator.childCount > 0) {
            pageIndicator.removeAllViews()
        }

        val dots = mutableListOf<ImageView>()

        for (i in 0..3) {
            dots.add(ImageView(this))

            if (i == view_pager.currentItem) {
                dots[i].setImageDrawable(getDrawable(R.drawable.selected_dot))
            } else {
                dots[i].setImageDrawable(getDrawable(R.drawable.default_dot))
            }

            val layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )

            layoutParams.setMargins(12, 0, 12, 0)

            pageIndicator.addView(dots[i], layoutParams)
        }
    }






}
