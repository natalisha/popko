package com.example.popko.profile

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.profile_item.view.*

class ProfileHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(profileItem: ProfileItem){
        itemView.tvContent.text = profileItem.content
        itemView.tvDescription.text = profileItem.descriptor
        itemView.ivIntentIcon.setImageDrawable(profileItem.icon)
        itemView.setOnClickListener(profileItem.onClickListener)
    }

}