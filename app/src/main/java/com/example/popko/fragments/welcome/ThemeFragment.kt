package com.example.popko.fragments.welcome

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.popko.R
import com.example.popko.data.SETTINGS_MODE
import kotlinx.android.synthetic.main.fragment_theme.*


class ThemeFragment : Fragment() {

    private lateinit var mSettings: SharedPreferences


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mSettings = PreferenceManager.getDefaultSharedPreferences(context)
        return inflater.inflate(R.layout.fragment_theme, container, false)
    }

    private fun setMode(isDarkMode: Boolean){
        if (isDarkMode){
            clDark.background = resources.getDrawable(R.drawable.choosen_rectangle)
//            clDark.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

        }else{
            clLight.background = resources.getDrawable(R.drawable.choosen_rectangle)
//            clLight.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        btnDarkTheme.isChecked = isDarkMode
        btnLightTheme.isChecked = !isDarkMode
        mSettings.edit().putBoolean(SETTINGS_MODE, isDarkMode).apply()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMode(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)

        clDark.setOnClickListener {
            setMode(true)
        }

        clLight.setOnClickListener {

            setMode(false)
        }

        btnDarkTheme.setOnClickListener {
            setMode(true)
        }

        btnLightTheme.setOnClickListener {
            setMode(false)
        }
    }



}



