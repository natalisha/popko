package com.example.popko.launcher

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.popko.R
import com.example.popko.icons.IconsClassAdapter
import com.example.popko.icons.LayoutModes
import com.example.popko.launcher.Launcher
import kotlinx.android.synthetic.main.app_bar_nav_drawer.*
import kotlinx.android.synthetic.main.fragment_linear_layout.*



class LinearFragment : Fragment() {

    private val itemType = 1

    private lateinit var iconListAdapter:IconsClassAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_linear_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = resources.getString(R.string.nav_linear)
        initList()
    }

    private fun initList() {
        iconListAdapter = IconsClassAdapter(LayoutModes.LINEAR_LAYOUT,
                (activity as Launcher).packageManager,(activity as Launcher).activityStarter)
        recyclerView.layoutManager = LinearLayoutManager(activity?.applicationContext)
        recyclerView.adapter = iconListAdapter
        iconListAdapter.icons = (activity as Launcher).getAppsList()
    }

}
