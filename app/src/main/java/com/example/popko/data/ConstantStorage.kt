package com.example.popko.data

const val SETTINGS_MODE = "daynight_mode"
const val SETTINGS_LAYOUT = "layout_model"
const val SETTINGS_START_PAGE = "welcome_page"

const val STANDARD_PORTR_DENSITY = 4
const val STANDARD_LAND_DENSITY = 6
const val TIGHT_PORTR_DENSITY = 5
const val TIGHT_LAND_DENSITY = 7



