package com.example.popko.icons.model

import androidx.annotation.LayoutRes
import com.example.popko.R
import com.example.popko.icons.LayoutModes

enum class IconModeler (
            val typeLayout: LayoutModes,
            @LayoutRes val layoutId: Int
    ) {
        Linear(LayoutModes.LINEAR_LAYOUT, R.layout.linear_item_view),
        TableTight(LayoutModes.TIGHT_LAYOUT, R.layout.table_layout_item),
        TableStandard(LayoutModes.STANDARD_LAYOUT, R.layout.table_item_view);
        companion object {

            fun byTypeInt(typeLayout: LayoutModes): IconModeler = values().first { it.typeLayout == typeLayout }
        }
}